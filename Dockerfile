FROM node:12-alpine

WORKDIR /app

COPY . /app/

RUN : \
    && rm -fr build \
    && yarn install \ 
    && yarn compile \
    ;

ENTRYPOINT yarn start