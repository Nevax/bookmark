import { MessageReaction, User, Client, Message, MessageEmbed } from "discord.js";

const client: Client = new Client();
const token: string = process.env.BOOKMARK_DISCORD_TOKEN;

if (token === undefined) {
  throw new Error("Token must be defined");
}

/**
 * The ready event is vital, it means that only _after_ this will your bot start reacting to information
 * received from Discord
 */
client.on("ready", () => {
  console.log("I am ready!");
});

client.on(
  "messageReactionAdd",
  (messageReaction: MessageReaction, user: User) => {
    if (
      messageReaction.emoji.name === "⭐" &&
      user.bot === false &&
      messageReaction.message.channel.type !== "dm"
    ) {
      if (messageReaction.message.embeds.length > 0) {
        messageReaction.message.embeds.forEach((embed : MessageEmbed) => {
          user.send({embed}).then((message: Message) => {
            message.react("❌");
          });
        });
      }

      if (messageReaction.message.content.length > 0) {
        user.send(messageReaction.message.content).then((message: Message) => {
          message.react("❌");
        });
      }
    }

    if (
      messageReaction.emoji.name === "❌" &&
      user.bot === false &&
      messageReaction.message.channel.type === "dm"
    ) {
      messageReaction.message.delete();
    }
  }
);

client.on("message", (message: Message) => {
  if (message.author.bot && message.channel.type !== "dm") {
    message.react("⭐");
  }
});

// Log our bot in using the token from https://discordapp.com/developers/applications/me
client.login(token);
